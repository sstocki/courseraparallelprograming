

package object scalashop {

  /** The value of every pixel is represented as a 32 bit integer. */
  type RGBA = Int

  /** Returns the red component. */
  def red(c: RGBA): Int = (0xff000000 & c) >>> 24

  /** Returns the green component. */
  def green(c: RGBA): Int = (0x00ff0000 & c) >>> 16

  /** Returns the blue component. */
  def blue(c: RGBA): Int = (0x0000ff00 & c) >>> 8

  /** Returns the alpha component. */
  def alpha(c: RGBA): Int = (0x000000ff & c) >>> 0

  /** Used to create an RGBA value from separate components. */
  def rgba(r: Int, g: Int, b: Int, a: Int): RGBA = {
    (r << 24) | (g << 16) | (b << 8) | (a << 0)
  }

  /** Restricts the integer into the specified range. */
  def clamp(v: Int, min: Int, max: Int): Int = {
    if (v < min) min
    else if (v > max) max
    else v
  }

  /** Image is a two-dimensional matrix of pixel values. */
  class Img(val width: Int, val height: Int, private val data: Array[RGBA]) {
    def this(w: Int, h: Int) = this(w, h, new Array(w * h))

    def apply(x: Int, y: Int): RGBA = data(y * width + x)

    def update(x: Int, y: Int, c: RGBA): Unit = data(y * width + x) = c

    def printData = print(data.mkString("", "; ", "\n"))
  }

  /** Computes the blurred RGBA value of a single pixel of the input image. */
  def boxBlurKernel(src: Img, x: Int, y: Int, radius: Int): RGBA = {
    // TODO implement using while loops
    if (radius == 0) src.apply(x, y)
    else {
      val xMax: Int = clamp(x + radius, 0, src.width - 1)
      val xMin: Int = clamp(x - radius, 0, src.width - 1)
      val yMax: Int = clamp(y + radius, 0, src.height - 1)
      val yMin: Int = clamp(y - radius, 0, src.height - 1)
      //println(s"x=<$xMin,$xMax>; y=<$yMin,$yMax>")
      val totalNoOfPixels: Int = ((1 + xMax - xMin) * (1 + yMax - yMin))
      //println(s"totalNoOfPixels=$totalNoOfPixels")
      var rSum: Int = 0
      var gSum: Int = 0
      var bSum: Int = 0
      var aSum: Int = 0
      var xx: Int = xMin
      var yy: Int = yMin
      while (xx <= xMax) {
        while (yy <= yMax) {
          //println(s"src.apply(xx, yy)=${src.apply(xx, yy)}")
          val rgba: RGBA = src.apply(xx, yy)
          rSum = rSum + red(rgba)
          gSum = gSum + green(rgba)
          bSum = bSum + blue(rgba)
          aSum = aSum + alpha(rgba)
          yy = yy + 1
        }
        yy = yMin
        xx = xx + 1
      }
      rgba(rSum / totalNoOfPixels, gSum / totalNoOfPixels, bSum / totalNoOfPixels, aSum / totalNoOfPixels)
    }
  }

}
