package scalashop

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class BlurSuite extends FunSuite {

  val src1 = new Img(3, 4)
  src1(0, 0) = 0; src1(1, 0) = 1; src1(2, 0) = 2
  src1(0, 1) = 3; src1(1, 1) = 4; src1(2, 1) = 5
  src1(0, 2) = 6; src1(1, 2) = 7; src1(2, 2) = 8
  src1(0, 3) = 50; src1(1, 3) = 11; src1(2, 3) = 16

  val src4_3 = new Img(4, 3)
  src4_3(0, 0) = 0; src4_3(1, 0) = 1; src4_3(2, 0) = 2; src4_3(3, 0) = 9
  src4_3(0, 1) = 3; src4_3(1, 1) = 4; src4_3(2, 1) = 5; src4_3(3, 1) = 10
  src4_3(0, 2) = 6; src4_3(1, 2) = 7; src4_3(2, 2) = 8; src4_3(3, 2) = 11

  val src3_3 = new Img(3, 3)
  src3_3(0, 0) = 0; src3_3(1, 0) = 1; src3_3(2, 0) = 2
  src3_3(0, 1) = 3; src3_3(1, 1) = 4; src3_3(2, 1) = 5
  src3_3(0, 2) = 6; src3_3(1, 2) = 7; src3_3(2, 2) = 8

  test("boxBlurKernel should correctly handle radius 0") {
    val src = new Img(5, 5)

    for (x <- 0 until 5; y <- 0 until 5)
      src(x, y) = rgba(x, y, x + y, math.abs(x - y))

    for (x <- 0 until 5; y <- 0 until 5)
      assert(boxBlurKernel(src, x, y, 0) === rgba(x, y, x + y, math.abs(x - y)),
        "boxBlurKernel(_,_,0) should be identity.")
  }

  test("boxBlurKernel should return the correct value on an interior pixel " +
    "of a 3x4 image with radius 1") {

    assert(boxBlurKernel(src1, 1, 2, 1) === 12,
      s"(boxBlurKernel(1, 2, 1) should be 12, " +
        s"but it's ${boxBlurKernel(src1, 1, 2, 1)})")
  }

  test("boxBlurKernel should return the correct value on a pixel at the y bottom border " +
    "of a 3x4 image with radius 1") {

    assert(boxBlurKernel(src1, 1, 3, 1) === 16,
      s"(boxBlurKernel(1, 2, 1) should be 16, " +
        s"but it's ${boxBlurKernel(src1, 1, 3, 1)})")
  }

  test("boxBlurKernel should return the correct value on a pixel at the y top border " +
    "of a 3x4 image with radius 1") {

    assert(boxBlurKernel(src1, 1, 0, 1) === 2,
      s"(boxBlurKernel(1, 2, 1) should be 2, " +
        s"but it's ${boxBlurKernel(src1, 1, 0, 1)})")
  }

  test("boxBlurKernel should return the correct value on a pixel at the x right border " +
    "of a 3x4 image with radius 1") {

    assert(boxBlurKernel(src1, 2, 1, 1) === 4,
      s"(boxBlurKernel(1, 2, 1) should be 4, " +
        s"but it's ${boxBlurKernel(src1, 2, 1, 1)})")
  }

  test("boxBlurKernel should return the correct value on a pixel at the x left border " +
    "of a 3x4 image with radius 1") {

    assert(boxBlurKernel(src1, 0, 2, 1) === 13,
      s"(boxBlurKernel(1, 2, 1) should be 13, " +
        s"but it's ${boxBlurKernel(src1, 0, 2, 1)})")
  }

  test("HorizontalBoxBlur.blur with radius 1 should correctly blur top two rows of the 3x3 image") {
    val w = 3
    val h = 3
    val src = src3_3
    val dst = new Img(w, h)

    HorizontalBoxBlur.blur(src, dst, 0, 2, 1)

    def check(x: Int, y: Int, expected: Int) =
      assert(dst(x, y) == expected, s"(destination($x, $y) should be $expected)")

    check(0, 0, 2)
    check(1, 0, 2)
    check(2, 0, 3)
    check(0, 1, 3)
    check(1, 1, 4)
    check(2, 1, 4)
    check(0, 2, 0)
    check(1, 2, 0)
    check(2, 2, 0)
  }

  test("HorizontalBoxBlur.blur with radius 1 should correctly blur the entire 3x3 image") {
    val w = 3
    val h = 3
    val src = src3_3
    val dst = new Img(w, h)

    HorizontalBoxBlur.blur(src, dst, 0, 3, 1)

    def check(x: Int, y: Int, expected: Int) =
      assert(dst(x, y) == expected, s"(destination($x, $y) should be $expected)")

    check(0, 0, 2)
    check(1, 0, 2)
    check(2, 0, 3)
    check(0, 1, 3)
    check(1, 1, 4)
    check(2, 1, 4)
    check(0, 2, 5)
    check(1, 2, 5)
    check(2, 2, 6)
  }

  test("HorizontalBoxBlur.parBlur with radius 1 should correctly blur the entire 3x3 image") {
    val w = 3
    val h = 3
    val src = src3_3
    val dst = new Img(w, h)

    HorizontalBoxBlur.parBlur(src, dst, h, 1)

    def check(x: Int, y: Int, expected: Int) =
      assert(dst(x, y) == expected, s"(destination($x, $y) should be $expected)")

    check(0, 0, 2)
    check(1, 0, 2)
    check(2, 0, 3)
    check(0, 1, 3)
    check(1, 1, 4)
    check(2, 1, 4)
    check(0, 2, 5)
    check(1, 2, 5)
    check(2, 2, 6)
  }

  test("HorizontalBoxBlur.parBlur with radius 1 and 32 tasks should correctly blur the entire 3x3 image") {
    val w = 3
    val h = 3
    val src = src3_3
    val dst = new Img(w, h)

    HorizontalBoxBlur.parBlur(src, dst, 32, 1)

    def check(x: Int, y: Int, expected: Int) =
      assert(dst(x, y) == expected, s"(destination($x, $y) should be $expected)")

    check(0, 0, 2)
    check(1, 0, 2)
    check(2, 0, 3)
    check(0, 1, 3)
    check(1, 1, 4)
    check(2, 1, 4)
    check(0, 2, 5)
    check(1, 2, 5)
    check(2, 2, 6)
  }

  test("VerticalBoxBlur.blur with radius 2 should correctly blur three left columns of the 4x3 image") {
    val w = 4
    val h = 3
    val src = src4_3
    val dst = new Img(w, h)

    VerticalBoxBlur.blur(src, dst, 0, 3, 2)

    def check(x: Int, y: Int, expected: Int) =
      assert(dst(x, y) == expected, s"(destination($x, $y) should be $expected)")

    check(0, 0, 4)
    check(1, 0, 5)
    check(2, 0, 5)
    check(3, 0, 0)
    check(0, 1, 4)
    check(1, 1, 5)
    check(2, 1, 5)
    check(3, 1, 0)
    check(0, 2, 4)
    check(1, 2, 5)
    check(2, 2, 5)
    check(3, 2, 0)
  }

  test("VerticalBoxBlur.blur with radius 2 should correctly blur the entire 4x3 image") {
    val w = 4
    val h = 3
    val src = src4_3
    val dst = new Img(w, h)

    VerticalBoxBlur.blur(src, dst, 0, 4, 2)

    def check(x: Int, y: Int, expected: Int) =
    assert(dst(x, y) == expected, s"(destination($x, $y) should be $expected)")

    check(0, 0, 4)
    check(1, 0, 5)
    check(2, 0, 5)
    check(3, 0, 6)
    check(0, 1, 4)
    check(1, 1, 5)
    check(2, 1, 5)
    check(3, 1, 6)
    check(0, 2, 4)
    check(1, 2, 5)
    check(2, 2, 5)
    check(3, 2, 6)
  }

  test("VerticalBoxBlur.parBlur with radius 2 should correctly blur the entire 4x3 image") {
    val w = 4
    val h = 3
    val src = src4_3
    val dst = new Img(w, h)

    VerticalBoxBlur.parBlur(src, dst, w, 2)

    def check(x: Int, y: Int, expected: Int) =
      assert(dst(x, y) == expected, s"(destination($x, $y) should be $expected)")

    check(0, 0, 4)
    check(1, 0, 5)
    check(2, 0, 5)
    check(3, 0, 6)
    check(0, 1, 4)
    check(1, 1, 5)
    check(2, 1, 5)
    check(3, 1, 6)
    check(0, 2, 4)
    check(1, 2, 5)
    check(2, 2, 5)
    check(3, 2, 6)
  }

  test("VerticalBoxBlur.parBlur with radius 2 and 32 tasks should correctly blur the entire 4x3 image") {
    val w = 4
    val h = 3
    val src = src4_3
    val dst = new Img(w, h)

    VerticalBoxBlur.parBlur(src, dst, 32, 2)

    def check(x: Int, y: Int, expected: Int) =
      assert(dst(x, y) == expected, s"(destination($x, $y) should be $expected)")

    check(0, 0, 4)
    check(1, 0, 5)
    check(2, 0, 5)
    check(3, 0, 6)
    check(0, 1, 4)
    check(1, 1, 5)
    check(2, 1, 5)
    check(3, 1, 6)
    check(0, 2, 4)
    check(1, 2, 5)
    check(2, 2, 5)
    check(3, 2, 6)
  }

  test("VerticalBoxBlur.getSplitPoints test for (11, 11) and 5 tasks") {
    val src = new Img(11, 11)
    val result = VerticalBoxBlur.getSplitPoints(src, 5)
    assert(result.length === 5)
    assert(result === List(0, 2, 4, 6, 8))
  }

  test("VerticalBoxBlur.getSplitPoints test for (11, 11) and 10 tasks") {
    val src = new Img(11, 11)
    val result = VerticalBoxBlur.getSplitPoints(src, 10)
    assert(result.length === 10)
    assert(result === List(0, 1, 2, 3, 4, 5, 6, 7, 8, 9))
  }

  test("VerticalBoxBlur.getSplitPoints test for (11, 11) and 4 tasks") {
    val src = new Img(11, 11)
    val result = VerticalBoxBlur.getSplitPoints(src, 4)
    assert(result.length === 4)
    assert(result === List(0, 3, 6, 9))
  }

  test("VerticalBoxBlur.getStartEndTuples test for (11, 11) and 5 tasks") {
    val src = new Img(11, 11)
    val result = VerticalBoxBlur.getStartEndTuples(src, 5)
    assert(result.length === 5)
    assert(result === List((0, 2), (2, 4), (4, 6), (6, 8), (8, 11)))
  }

  test("VerticalBoxBlur.getStartEndTuples test for (11, 11) and 10 tasks") {
    val src = new Img(11, 11)
    val result = VerticalBoxBlur.getStartEndTuples(src, 10)
    assert(result.length === 10)
    assert(result === List((0, 1), (1, 2), (2, 3), (3, 4), (4, 5), (5, 6), (6, 7), (7, 8), (8, 9), (9, 11)))
  }

  test("VerticalBoxBlur.getStartEndTuples test for (11, 11) and 4 tasks") {
    val src = new Img(11, 11)
    val result = VerticalBoxBlur.getStartEndTuples(src, 4)
    assert(result.length === 4)
    assert(result === List((0,3), (3,6), (6,9), (9,11)))
  }

  test("VerticalBoxBlur.getStartEndTuples test for (4, 3) and 4 tasks") {
    val src = new Img(4, 3)
    val result: List[(Int, Int)] = VerticalBoxBlur.getStartEndTuples(src, 4)
    assert(result.length === 4)
    assert(result === List((0,1), (1,2), (2,3), (3,4)))
  }

  test("VerticalBoxBlur.getStep test for (4, 3) and 20 tasks") {
    val src = new Img(4, 3)
    val result: Int = VerticalBoxBlur.getStep(src, 20)
    assert(result === 1)
  }
}
