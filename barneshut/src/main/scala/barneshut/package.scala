import common._
import barneshut.conctrees._

package object barneshut {

  class Boundaries {
    var minX = Float.MaxValue

    var minY = Float.MaxValue

    var maxX = Float.MinValue

    var maxY = Float.MinValue

    def width = maxX - minX

    def height = maxY - minY

    def size = math.max(width, height)

    def centerX = minX + width / 2

    def centerY = minY + height / 2

    override def toString = s"Boundaries($minX, $minY, $maxX, $maxY)"
  }

  sealed abstract class Quad {

    /**
      * The X coordinate of the center of mass of the bodies in the respective cell.
      *
      * @return massX
      */
    def massX: Float

    /**
      * The Y coordinate of the center of mass of the bodies in the respective cell.
      *
      * @return mssY
      */
    def massY: Float

    /**
      * The total mass of bodies in that cell.
      *
      * @return mass
      */
    def mass: Float

    /**
      * The X coordinate of the center of the cell.
      *
      * @return centerX
      */
    def centerX: Float

    /**
      * The Y coordinate of the center of the cell.
      *
      * @return centerY
      */
    def centerY: Float

    /**
      * Length of the side of the cell
      *
      * @return size
      */
    def size: Float

    /**
      * Total number of bodies in the cell.
      *
      * @return total
      */
    def total: Int

    def insert(b: Body): Quad
  }

  case class Empty(centerX: Float, centerY: Float, size: Float) extends Quad {
    def massX: Float = centerX

    def massY: Float = centerY

    def mass: Float = 0

    def total: Int = 0

    def insert(b: Body): Quad = {

      //println("Empty: insert")

      Leaf(
        centerX = centerX,
        centerY = centerY,
        size = size,
        bodies = Seq(b)
      )
    }
  }

  /**
    * The children nodes that represent four adjacent cells of the same size and adjacent to each other.
    *
    * @param nw top left cell
    * @param ne top right cell
    * @param sw bottom left cell
    * @param se bottom right cell
    */
  case class Fork(
                   nw: Quad, ne: Quad, sw: Quad, se: Quad
                 ) extends Quad {
    val centerX: Float = nw.centerX + (nw.size / 2)
    val centerY: Float = nw.centerY + (nw.size / 2)
    val size: Float = nw.size
    val mass: Float = nw.mass + ne.mass + sw.mass + se.mass

    val massX: Float = calculateMassCenter(centerX, qt => qt.mass * qt.massX)
    val massY: Float = calculateMassCenter(centerY, qt => qt.mass * qt.massY)
    val total: Int = nw.total + ne.total + sw.total + se.total

    private def fourQuadTrees: Seq[Quad] = Seq(nw, ne, sw, se)

    private def calculateMassCenter(default: Float, f: Quad => Float): Float = {
      if (mass == 0) default else fourQuadTrees.map(f).sum / mass
    }

    def insert(b: Body): Fork = {

      //println("Fork: insert")

      def isInside(quad: Quad, body: Body): Boolean = {
        val d = size / 2
        def minX: Float = quad.centerX - d
        def maxX: Float = quad.centerX + d
        def minY: Float = quad.centerY - d
        def maxY: Float = quad.centerY + d

        val result = body.x >= minX && body.x <= maxX && body.y >= minY && body.y <= maxY
        //println(s"isInside=$result; ($minX, $maxX), ($minY, $maxY); body=$body")
        result
      }

      if (isInside(nw, b)) Fork(nw.insert(b), ne, sw, se)
      else if (isInside(ne, b)) Fork(nw, ne.insert(b), sw, se)
      else if (isInside(sw, b)) Fork(nw, ne, sw.insert(b), se)
      else if (isInside(se, b)) Fork(nw, ne, sw, se.insert(b))
      else this
    }
  }

  case class Leaf(centerX: Float, centerY: Float, size: Float, bodies: Seq[Body])
    extends Quad {
    val mass: Float = bodies.map(_.mass).sum
    val massX: Float = calculateMassCenter(centerX, body => body.mass * body.x)
    val massY: Float = calculateMassCenter(centerY, body => body.mass * body.y)
    val total: Int = bodies.length

    private def calculateMassCenter(default: Float, f: Body => Float): Float = bodies match {
      case Seq() => default
      case _ => bodies.map(f).sum / mass
    }

    def insert(b: Body): Quad = {

      //println("Leaf: insert")

      def iter(quad: Quad, bodySeq: Seq[Body]): Quad = bodySeq match {
        case Seq() => quad
        case head +: tail => iter(quad.insert(head), tail)
      }

      if (size > minimumSize) {
        val newSize = size / 2
        val d = size / 4
        val nw: Quad = Empty(centerX - d, centerY - d, newSize)
        val ne: Quad = Empty(centerX + d, centerY - d, newSize)
        val sw: Quad = Empty(centerX - d, centerY + d, newSize)
        val se: Quad = Empty(centerX + d, centerY + d, newSize)
        val fork: Quad = Fork(nw, ne, sw, se)
        iter(fork, bodies :+ b)
      } else Leaf(
        centerX = centerX,
        centerY = centerY,
        size = size,
        bodies = bodies :+ b
      )
    }
  }

  def minimumSize = 0.00001f

  def gee: Float = 100.0f

  def delta: Float = 0.01f

  def theta = 0.5f

  def eliminationThreshold = 0.5f

  def force(m1: Float, m2: Float, dist: Float): Float = gee * m1 * m2 / (dist * dist)

  def distance(x0: Float, y0: Float, x1: Float, y1: Float): Float = {
    math.sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0)).toFloat
  }

  class Body(val mass: Float, val x: Float, val y: Float, val xspeed: Float, val yspeed: Float) {

    override def toString = s"Body($mass, $x, $y, $xspeed, $yspeed)"

    def updated(quad: Quad): Body = {
      var netforcex = 0.0f
      var netforcey = 0.0f

      def addForce(thatMass: Float, thatMassX: Float, thatMassY: Float): Unit = {
        val dist = distance(thatMassX, thatMassY, x, y)
        /* If the distance is smaller than 1f, we enter the realm of close
         * body interactions. Since we do not model them in this simplistic
         * implementation, bodies at extreme proximities get a huge acceleration,
         * and are catapulted from each other's gravitational pull at extreme
         * velocities (something like this:
         * http://en.wikipedia.org/wiki/Interplanetary_spaceflight#Gravitational_slingshot).
         * To decrease the effect of this gravitational slingshot, as a very
         * simple approximation, we ignore gravity at extreme proximities.
         */
        if (dist > 1f) {
          val dforce = force(mass, thatMass, dist)
          val xn = (thatMassX - x) / dist
          val yn = (thatMassY - y) / dist
          val dforcex = dforce * xn
          val dforcey = dforce * yn
          netforcex += dforcex
          netforcey += dforcey
        }
      }

      def traverse(quad: Quad): Unit = (quad: Quad) match {
        case Empty(_, _, _) =>
        // no force
        case Leaf(_, _, _, bodies) =>
          // add force contribution of each body by calling addForce
          bodies.map(body => addForce(body.mass, body.x, body.y))
        case Fork(nw, ne, sw, se) =>
          val dist = distance(quad.massX, quad.massY, x, y)
          // see if node is far enough from the body,
          if (quad.size / dist < theta) addForce(quad.mass, quad.massX, quad.massY)
          // or recursion is needed
          else {
            traverse(nw)
            traverse(ne)
            traverse(sw)
            traverse(se)
          }
      }

      traverse(quad)

      val nx = x + xspeed * delta
      val ny = y + yspeed * delta
      val nxspeed = xspeed + netforcex / mass * delta
      val nyspeed = yspeed + netforcey / mass * delta

      new Body(mass, nx, ny, nxspeed, nyspeed)
    }

  }

  val SECTOR_PRECISION = 8

  class SectorMatrix(val boundaries: Boundaries, val sectorPrecision: Int) {
    val sectorSize: Float = boundaries.size / sectorPrecision
    val matrix = new Array[ConcBuffer[Body]](sectorPrecision * sectorPrecision)
    for (i <- 0 until matrix.length) matrix(i) = new ConcBuffer

    def +=(b: Body): SectorMatrix = {

      println(s"body=$b")

      def insideBoundaries: Boolean = b.x >= boundaries.minX && b.x <= boundaries.maxX &&
        b.y >= boundaries.minY && b.y <= boundaries.maxY

      def moveInsideBoundaries: Body = {
        val newX = {
          if (b.x < boundaries.minX) boundaries.minX
          else if (b.x > boundaries.maxX) boundaries.maxX
          else b.x
        }
        val newY = {
          if (b.y < boundaries.minY) boundaries.minY
          else if (b.y > boundaries.maxY) boundaries.maxY
          else b.y
        }
        new Body(b.mass, newX, newY, b.xspeed, b.yspeed)
      }

      val body: Body = if (insideBoundaries) b else moveInsideBoundaries

      // check if body lies inside the boundaries
      val relativeX = body.x - boundaries.minX
      val relativeY = body.y - boundaries.minY
      val col = (relativeX / sectorSize).toInt
      val row = (relativeY / sectorSize).toInt
      val index = row * sectorPrecision + col

      println(s"(relativeX,relativeY)=($relativeX,$relativeY)")
      println(s"(col,row)=($col,$row)")
      println(s"index=$index")

      matrix(index) += b
      this
    }

    def apply(x: Int, y: Int) = matrix(y * sectorPrecision + x)

    def combine(that: SectorMatrix): SectorMatrix = {
      val resultSectorMatrix = new SectorMatrix(that.boundaries, that.sectorPrecision)
      for(i <- 0 until matrix.length) {
        resultSectorMatrix.matrix(i) = this.matrix(i).combine(that.matrix(i))
      }
      resultSectorMatrix
    }

    def toQuad(parallelism: Int): Quad = {
      def BALANCING_FACTOR = 4
      def quad(x: Int, y: Int, span: Int, achievedParallelism: Int): Quad = {
        if (span == 1) {
          val sectorSize = boundaries.size / sectorPrecision
          val centerX = boundaries.minX + x * sectorSize + sectorSize / 2
          val centerY = boundaries.minY + y * sectorSize + sectorSize / 2
          var emptyQuad: Quad = Empty(centerX, centerY, sectorSize)
          val sectorBodies = this (x, y)
          sectorBodies.foldLeft(emptyQuad)(_ insert _)
        } else {
          val nspan = span / 2
          val nAchievedParallelism = achievedParallelism * 4
          val (nw, ne, sw, se) =
            if (parallelism > 1 && achievedParallelism < parallelism * BALANCING_FACTOR) parallel(
              quad(x, y, nspan, nAchievedParallelism),
              quad(x + nspan, y, nspan, nAchievedParallelism),
              quad(x, y + nspan, nspan, nAchievedParallelism),
              quad(x + nspan, y + nspan, nspan, nAchievedParallelism)
            )
            else (
              quad(x, y, nspan, nAchievedParallelism),
              quad(x + nspan, y, nspan, nAchievedParallelism),
              quad(x, y + nspan, nspan, nAchievedParallelism),
              quad(x + nspan, y + nspan, nspan, nAchievedParallelism)
              )
          Fork(nw, ne, sw, se)
        }
      }

      quad(0, 0, sectorPrecision, 1)
    }

    override def toString = s"SectorMatrix(#bodies: ${matrix.map(_.size).sum})"
  }

  class TimeStatistics {
    private val timeMap = collection.mutable.Map[String, (Double, Int)]()

    def clear() = timeMap.clear()

    def timed[T](title: String)(body: => T): T = {
      var res: T = null.asInstanceOf[T]
      val totalTime = /*measure*/ {
        val startTime = System.currentTimeMillis()
        res = body
        (System.currentTimeMillis() - startTime)
      }

      timeMap.get(title) match {
        case Some((total, num)) => timeMap(title) = (total + totalTime, num + 1)
        case None => timeMap(title) = (0.0, 0)
      }

      println(s"$title: ${totalTime} ms; avg: ${timeMap(title)._1 / timeMap(title)._2}")
      res
    }

    override def toString = {
      timeMap map {
        case (k, (total, num)) => k + ": " + (total / num * 100).toInt / 100.0 + " ms"
      } mkString ("\n")
    }
  }

}
