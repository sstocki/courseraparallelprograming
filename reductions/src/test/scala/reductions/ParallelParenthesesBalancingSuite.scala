package reductions

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import reductions.ParallelParenthesesBalancing._

@RunWith(classOf[JUnitRunner])
class ParallelParenthesesBalancingSuite extends FunSuite {

  val balanced: Boolean = true
  val notBalanced: Boolean = false
  
  test("balance should work for empty string") {
    def check(input: String, expected: Boolean) =
      assert(balance(input.toArray) == expected,
        s"balance($input) should be $expected")

    check("", balanced)
  }

  test("balance should work for string of length 1") {
    def check(input: String, expected: Boolean) =
      assert(balance(input.toArray) == expected,
        s"balance($input) should be $expected")

    check("(", notBalanced)
    check(")", notBalanced)
    check(".", balanced)
  }

  test("balance should work for string of length 2") {
    def check(input: String, expected: Boolean) =
      assert(balance(input.toArray) == expected,
        s"balance($input) should be $expected")

    check("()", balanced)
    check(")(", notBalanced)
    check("((", notBalanced)
    check("))", notBalanced)
    check(".)", notBalanced)
    check(".(", notBalanced)
    check("(.", notBalanced)
    check(").", notBalanced)
  }

  test("parBalance should work for string of length 2 and threshold 1") {
    def check(input: String, expected: Boolean) =
      assert(parBalance(input.toArray, 1) == expected,
        s"parBalance($input) should be $expected")

    check("()", balanced)
    check(")(", notBalanced)
    check("((", notBalanced)
    check("))", notBalanced)
    check(".)", notBalanced)
    check(".(", notBalanced)
    check("(.", notBalanced)
    check(").", notBalanced)
  }


}