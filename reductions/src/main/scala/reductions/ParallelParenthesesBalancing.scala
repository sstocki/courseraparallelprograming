package reductions

import common._
import org.scalameter._

import scala.annotation.tailrec

object ParallelParenthesesBalancingRunner {

  @volatile var seqResult = false

  @volatile var parResult = false

  val standardConfig = config(
    Key.exec.minWarmupRuns -> 20,
    Key.exec.maxWarmupRuns -> 30,
    Key.exec.benchRuns -> 40,
    Key.verbose -> true
  ) withWarmer new Warmer.Default

  def main(args: Array[String]): Unit = {
    val length = 1000000
    val chars = new Array[Char](length)
    val threshold = 10000
    val seqtime = standardConfig measure {
      seqResult = ParallelParenthesesBalancing.balance(chars)
    }
    println(s"sequential result = $seqResult")
    println(s"sequential balancing time: $seqtime ms")

    val fjtime = standardConfig measure {
      parResult = ParallelParenthesesBalancing.parBalance(chars, threshold)
    }
    println(s"sequential result = $seqResult")
    println(s"sequential balancing time: $seqtime ms")
    println(s"parallel result = $parResult")
    println(s"parallel balancing time: $fjtime ms")
    println(s"speedup: ${seqtime / fjtime}")


    //test
//    val testStringBalanced1 = "I told him (that it's not (yet) done). (But he wasn't listening)"
//    val testStringBalanced2 = "(if (zero? x) max (/ 1 x))"
//    val testStringNotBalanced1 = ")()("
//    val testStringNotBalanced2 = "())("
//
//    val resultBalanced1 = ParallelParenthesesBalancing.parBalance(testStringBalanced1.toCharArray, 5)
//    println(s"$testStringBalanced1 = $resultBalanced1 [${ParallelParenthesesBalancing.balance(testStringBalanced1.toCharArray)}]")
//
//    val resultBalanced2 = ParallelParenthesesBalancing.parBalance(testStringBalanced2.toCharArray, 5)
//    println(s"$testStringBalanced2 = $resultBalanced2 [${ParallelParenthesesBalancing.balance(testStringBalanced2.toCharArray)}]")
//
//    val resultNotBalanced1 = ParallelParenthesesBalancing.parBalance(testStringNotBalanced1.toCharArray, 5)
//    println(s"$testStringNotBalanced1 = $resultNotBalanced1 [${ParallelParenthesesBalancing.balance(testStringNotBalanced1.toCharArray)}]")
//
//    val resultNotBalanced2 = ParallelParenthesesBalancing.parBalance(testStringNotBalanced2.toCharArray, 5)
//    println(s"$testStringNotBalanced2 = $resultNotBalanced2 [${ParallelParenthesesBalancing.balance(testStringNotBalanced2.toCharArray)}]")
//
//    val test1 = ")((()_("
//    val result1 = ParallelParenthesesBalancing.traverseTest(test1.toCharArray, 0, test1.length)
//    println(s"result1=$result1")
//
//    val test2 = "()(_(("
//    val result2 = ParallelParenthesesBalancing.traverseTest(test2.toCharArray, 0, test2.length)
//    println(s"result2=$result2")
//
//    val test3 = "). ("
//    val result3 = ParallelParenthesesBalancing.traverseTest(test3.toCharArray, 0, test3.length)
//    println(s"result3=$result3")
//
//    println(s"merge((0,1),(2,3))=${ParallelParenthesesBalancing.mergeTest((0,1), (2,3))}")
//    println(s"merge((5,3),(1,3))=${ParallelParenthesesBalancing.mergeTest((5,3), (1,3))}")
//    println(s"merge((2,1),(3,1))=${ParallelParenthesesBalancing.mergeTest((2,1), (3,1))}")
  }
}

object ParallelParenthesesBalancing {

  /** Returns `true` iff the parentheses in the input `chars` are balanced.
   */
  def balance(chars: Array[Char]): Boolean = {

    @tailrec
    def iterate(charList: List[Char], balance: Int): Int = {
      charList match {
        case List() => balance
        case c :: tail =>
          val balancingFactor: Int = balance + {
            if (c == '(') 1
            else if (c == ')') -1
            else 0
          }
          if(balancingFactor < 0) balancingFactor
          else iterate(tail, balancingFactor)
      }
    }

    iterate(chars.toList, 0) == 0
  }

  /** Returns `true` iff the parentheses in the input `chars` are balanced.
   */
  def parBalance(chars: Array[Char], threshold: Int): Boolean = {

    def traverse(idx: Int, until: Int, arg1: Int, arg2: Int): (Int, Int) = {
      var openParenthesesCount = 0
      var closeParenthesesCount = 0
      var i = idx
      while (i < until) {
        val c = chars(i)
        if (c == '(') {
          openParenthesesCount = openParenthesesCount + 1
        }
        else if (c == ')') {
          if(openParenthesesCount > 0) openParenthesesCount = openParenthesesCount - 1
          else closeParenthesesCount = closeParenthesesCount  + 1
        }
        i = i + 1
      }
      val res: (Int, Int) = (closeParenthesesCount, openParenthesesCount)
      //println(s"""traverse("${chars.slice(idx, until).mkString}") = $res""")
      res
    }

    def merge(first: (Int, Int), second: (Int, Int)): (Int, Int) = {
      val (l1, r1) = first
      val (l2, r2) = second
      if(l2 > r1) (l1 + l2 - r1, r2)
      else (l1, r1 + r2 - l2)
    }

    def reduce(from: Int, until: Int): (Int, Int) = {
      //println(s"reduce:${chars.slice(from, until).mkString("")}")
      if (until - from <= threshold) {
        traverse(from, until, 0, 0)
      } else {
        val mid = from + (until - from) / 2
        val (a1, a2) = parallel(reduce(from, mid), reduce(mid, until))
        val mergeResult = merge(a1, a2)
        //println(s"merge($a1, $a2)=$mergeResult")
        mergeResult
      }
    }

    reduce(0, chars.length) == (0, 0)
  }

  def traverseTest(chars: Array[Char], idx: Int, until: Int): (Int, Int) = {
    var openParenthesesCount = 0
    var closeParenthesesCount = 0
    var i = idx
    while (i < until) {
      val c = chars(i)
      if (c == '(') {
        openParenthesesCount = openParenthesesCount + 1
      }
      else if (c == ')') {
        if(openParenthesesCount > 0) openParenthesesCount = openParenthesesCount - 1
        else closeParenthesesCount = closeParenthesesCount + 1
      }
      i = i + 1
    }
    val res: (Int, Int) = (closeParenthesesCount, openParenthesesCount)
    println(s"""traverse("${chars.slice(idx, until).mkString}") = $res""")
    res
  }

  def mergeTest(first: (Int, Int), second: (Int, Int)): (Int, Int) = {
    val (l1, r1) = first
    val (l2, r2) = second
    if(l2 > r1) (l1 + l2 - r1, r2)
    else (l1, r1 + r2 - l2)
  }

  // For those who want more:
  // Prove that your reduction operator is associative!

}
