package reductions

import org.scalameter._
import common._

object ParallelCountChangeRunner {

  @volatile var seqResult = 0

  @volatile var parResult = 0

  val standardConfig = config(
    Key.exec.minWarmupRuns -> 10,
    Key.exec.maxWarmupRuns -> 20,
    Key.exec.benchRuns -> 20,
    Key.verbose -> true
  ) withWarmer(new Warmer.Default)

  def main(args: Array[String]): Unit = {
    val amount = 500
    val coins = List(1, 2, 5, 10, 20, 50, 100)
    val seqtime = standardConfig measure {
      seqResult = ParallelCountChange.countChange(amount, coins)
    }
    println(s"sequential result = $seqResult")
    println(s"sequential count time: $seqtime ms")

    def measureParallelCountChange(threshold: ParallelCountChange.Threshold): Unit = {
      val fjtime = standardConfig measure {
        parResult = ParallelCountChange.parCountChange(amount, coins, threshold)
      }
      println(s"sequential result = $seqResult")
      println(s"sequential count time: $seqtime ms")
      println(s"parallel result = $parResult")
      println(s"parallel count time: $fjtime ms")
      println(s"speedup: ${seqtime / fjtime}")
    }

    measureParallelCountChange(ParallelCountChange.moneyThreshold(amount))
    measureParallelCountChange(ParallelCountChange.totalCoinsThreshold(coins.length))
    measureParallelCountChange(ParallelCountChange.combinedThreshold(amount, coins))
  }
}

object ParallelCountChange {

  /** Returns the number of ways change can be made from the specified list of
   *  coins for the specified amount of money.
   */
  def countChange(money: Int, coins: List[Int]): Int = {

    def iterate(moneyLeft: Int, coinsToUse: List[Int], numberOfWays: Int): Int = {

      if(moneyLeft == 0) numberOfWays + 1
      else if(coinsToUse.isEmpty || moneyLeft < 0) numberOfWays
      else {
        iterate(moneyLeft - coinsToUse.head, coinsToUse, numberOfWays) + iterate(moneyLeft, coinsToUse.tail, numberOfWays)
      }
    }

    if (money == 0) 1
    else if (coins.isEmpty) 0
    else iterate(money, coins, 0)
  }

  type Threshold = (Int, List[Int]) => Boolean

  /** In parallel, counts the number of ways change can be made from the
   *  specified list of coins for the specified amount of money.
   */
  def parCountChange(money: Int, coins: List[Int], threshold: Threshold): Int = {

    def iteratePar(moneyLeft: Int, coinsToUse: List[Int], threshold: Threshold, numberOfWays: Int): Int = {

      if(moneyLeft == 0) numberOfWays + 1
      else if(coinsToUse.isEmpty || moneyLeft < 0) numberOfWays
      else {
        val (v1, v2) = {
          if (threshold(moneyLeft, coinsToUse)) {
            (iteratePar(moneyLeft - coinsToUse.head, coinsToUse, threshold, numberOfWays),
              iteratePar(moneyLeft, coinsToUse.tail, threshold, numberOfWays))
          } else {
            parallel(iteratePar(moneyLeft - coinsToUse.head, coinsToUse, threshold, numberOfWays),
              iteratePar(moneyLeft, coinsToUse.tail, threshold, numberOfWays))
          }
        }
        v1 + v2
      }
    }

    if (money == 0) 1
    else if (coins.isEmpty) 0
    else iteratePar(money, coins, threshold, 0)
  }

  /** Threshold heuristic based on the starting money. */
  def moneyThreshold(startingMoney: Int): Threshold = (moneyLeft, _) => moneyLeft <= 2 * startingMoney / 3

  /** Threshold heuristic based on the total number of initial coins. */
  def totalCoinsThreshold(totalCoins: Int): Threshold = (_, coinsToUse) => coinsToUse.length <= 2 * totalCoins / 3


  /** Threshold heuristic based on the starting money and the initial list of coins. */
  def combinedThreshold(startingMoney: Int, allCoins: List[Int]): Threshold = (moneyLeft, coinsToUse) =>
    (moneyLeft * coinsToUse.length) <= (startingMoney * allCoins.length) / 2
}
