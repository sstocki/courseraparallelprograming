package silverstar
package coursera.parallel.programming
package week4

/**
  * My list.
  * List is not suitable for parallel computation.
  */
sealed trait MyList[+T] {

  def head: T
  def tail: MyList[T]
}

case class ::[T](head: T, tail: MyList[T]) extends MyList[T]

case object Nil extends MyList[Nothing] {
  def head = sys.error("empty list")
  def tail = sys.error("empty list")
}

object MyListRunner {

  def filter[T](list: MyList[T])(predicate: T => Boolean): MyList[T] = list match {
    case x :: xs if predicate(x) => ::(x, filter(xs)(predicate))
    case x :: xs => filter(xs)(predicate)
    case Nil => Nil
  }

  def main(args: Array[String]) {

    val myList: MyList[Int] = ::(2, ::(4, ::(7, ::(8, Nil))))

  }
}
