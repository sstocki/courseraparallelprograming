package silverstar
package coursera.parallel.programming
package week4

import scala.reflect.ClassTag

/**
  * Conc buffer. Combiner implementation for Conc.Conc buffer. Combiner implementation for Conc.
  *
  * @param k size of an array
  * @param conc conc
  */
class ConcBuffer[T: ClassTag](val k : Int, private var conc: Conc[T]) {

  /**
    * When array is full it is pushed to the conc as a Leaf and a new array is allocated.
    */
  private var chunk: Array[T] = new Array(k)
  private var chunkSize: Int = 0

  def result: Conc[T] = {
    conc = Conc.appendLeaf(conc, new Chunk(chunk, chunkSize))
    conc
  }

  final def +=(elem: T): ConcBuffer[T] = {
    if (chunkSize >= k) expand()
    chunk(chunkSize) = elem
    chunkSize += 1
    this
  }

  private def expand() {
    conc = Conc.appendLeaf(conc, new Chunk(chunk, chunkSize))
    chunk = new Array(k)
    chunkSize = 0
  }

  final def combine(that: ConcBuffer[T]): ConcBuffer[T] = {
    val combinedConc = this.result <> that.result
    new ConcBuffer(k, combinedConc)
  }
}

object ConcBuffer {

  def main(args: Array[String]) {

    val array: Array[String] = Array[String]("a", "r", "v", "w", "b", "s", "n")

    val combine1: Conc[String] = array.par.aggregate(new ConcBuffer[String](4, Empty))(_ += _, _ combine _).result
  }
}
