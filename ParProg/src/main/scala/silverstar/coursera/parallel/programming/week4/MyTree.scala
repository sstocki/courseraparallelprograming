package silverstar
package coursera.parallel.programming
package week4

import common._

/**
  * My tree.
  * Tree is suitable for parallel computation only if it is balanced.
  */
sealed trait MyTree[+T]

case class MyNode[T](left: MyTree[T], right: MyTree[T]) extends MyTree[T]

case class MyLeaf[T](elem: T) extends MyTree[T]

case object MyEmpty extends MyTree[Nothing]

object MyTreeRunner {

  def filter[T](t: MyTree[T])(predicate: T => Boolean): MyTree[T] = t match {
    case MyNode(left, right) =>
      val (l, r) = parallel(filter(left)(predicate), filter(right)(predicate))
      MyNode(l, r)
    case MyLeaf(elem) => if(predicate(elem)) t else MyEmpty
    case MyEmpty => MyEmpty
  }

  def main(args: Array[String]) {

    val myTree: MyNode[Int] = MyNode(MyNode(MyNode(MyLeaf(1), MyLeaf(2)), MyNode(MyLeaf(3), MyLeaf(4))), MyNode(MyLeaf(5), MyLeaf(6)))

    val oddTree: MyTree[Int] = filter(myTree)(_ % 2 == 1)
  }

}