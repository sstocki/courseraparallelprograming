package silverstar
package coursera.parallel.programming
package week1

/**
  * Hello therad.
  */
class HelloThread extends Thread {

  override def run(): Unit = {
    println("Hello world")
  }
}

object HelloThread {

  def main(args: Array[String]) {
    val t = new HelloThread

    t.start()
    t.join()
  }
}
