package silverstar
package coursera.parallel.programming
package week2

import common._

/**
  * Parallel map.
  */
object ParallelMap {

  def mapSeq[A, B](list: List[A], f: A => B): List[B] = list match {
    case List() => List()
    case head :: tail => f(head) :: mapSeq(tail, f)
  }

  /**
    * Sequential version of mapping a segment of a array.
    * We use arrays because they are efficient for parallelism. We can easy access a specific element of a List.
    * It writes to output for left <= i < right
    *
    * @param input input array
    * @param output output array (result of mapping)
    * @param left left
    * @param right right
    * @param f mapping function
    * @tparam A type A
    * @tparam B type B
    * @return unit
    */
  def mapArraySegmentSeq[A, B](input: Array[A], output: Array[B], left: Int, right: Int, f: A => B): Unit = {
    var i = left
    while (i < right) {
      output(i) = f(input(i))
      i = i + 1
    }
  }

  /**
    * Parallel version of mapping a segment of a array.
    *
    * @param input  input array
    * @param output output array (result of mapping)
    * @param left left
    * @param right right
    * @param f      mapping function
    * @tparam A type A
    * @tparam B type B
    * @return unit
    */
  def mapArraySegmentPar[A, B](input: Array[A], output: Array[B], left: Int, right: Int, f: A => B): Unit = {
    val threshold: Int = 5
    if (right - left < threshold) {
      mapArraySegmentSeq(input, output, left, right, f)
    } else {
      val mid = left + (right - left) / 2
      parallel(mapArraySegmentPar(input, output, left, mid, f), mapArraySegmentPar(input, output, mid, right, f))
    }
  }

  def main(args: Array[String]) {

    def testMapping: (Int) => String = (x: Int) => (x*x).toString
    val testArray: Array[Int] = (1 to 50 by 2).toArray
    val mapSeqResult: Array[String] = mapSeq(testArray.toList, testMapping).toArray

    val outputArray1: Array[String] = new Array[String](testArray.length)
    mapArraySegmentSeq(testArray, outputArray1, 0, testArray.length, testMapping)
    val mapArraySegmentSeqResult: Array[String] = outputArray1

    val outputArray2: Array[String] = new Array[String](testArray.length)
    mapArraySegmentPar(testArray, outputArray2, 0, testArray.length, testMapping)
    val mapArraySegmentParResult: Array[String] = outputArray1

    println(s"testArray=${testArray.mkString("; ")}")
    println(s"mapSeqResult=${mapSeqResult.mkString("; ")}")
    println(s"mapArraySegmentSeqResult=${mapArraySegmentSeqResult.mkString("; ")}")
    println(s"mapArraySegmentParResult=${mapArraySegmentParResult.mkString("; ")}")
  }

}
