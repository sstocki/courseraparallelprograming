package silverstar
package coursera.parallel.programming
package week4

import scala.annotation.tailrec

/**
  * Conc represents balanced tree..
  * It requires tree to be balanced while creting the tree.
  */
sealed trait Conc[+T] {

  /**
    * Longest path from the root to the leaf. Height of the tree.
    *
    * @return level
    */
  def level: Int

  /**
    * Number of elements in the tree.
    *
    * @return size
    */
  def size: Int

  /**
    * Left subtree
    *
    * @return left subtree
    */
  def left: Conc[T]

  /**
    * Right subtree
    *
    * @return right subtree
    */
  def right: Conc[T]

  def <>(that: Conc[T]): Conc[T] = {
    if (this == Empty) that
    else if (that == Empty) this
    else concat(this, that)
  }

  def concat[T](xs: Conc[T], ys: Conc[T]): Conc[T] = {
    val levelDiff: Int = ys.level - xs.level
    if(math.abs(levelDiff) <= 1) new <>(xs, ys)
    // left subtree is higher than right
    else if(levelDiff < -1) {
      if(xs.left.level >= xs.right.level) {
        val nr = concat(xs.right, ys)
        new <>(xs.left, nr)
      } else {
        val nrr = concat(xs.right.right, ys)
        if (nrr.level == xs.level - 3) {
          val nl = xs.left
          val nr = new <>(xs.right.left, nrr)
          new <>(nl, nr)
        } else {
          val nl = new <>(xs.left, xs.right.left)
          val nr = nrr
          new <>(nl, nr)
        }
      }
    }
    // right subtree is higher than left
    else {
//      if(ys.right.level >= ys.left.level) {
//        val nr = concat(xs, ys.left)
//        new <>(nr, ys.right)
//      } else {
//        val nrr = concat(xs, ys.left.left)
//        if (nrr.level == ys.level - 3) {
//          val nl = ys.left
//          val nr = new <>(nrr, ys.left.right)
//          new <>(nl, nr)
//        } else {
//          val nl = new <>(xs.left, xs.right.left)
//          val nr = nrr
//          new <>(nl, nr)
//        }
//      }
      ???
    }
  }

}

case object Empty extends Conc[Nothing] {
  override def level: Int = 0
  override def size: Int = 0

  override def left: Conc[Nothing] = sys.error("empty conc")
  override def right: Conc[Nothing] = sys.error("empty conc")
}

case class Single[T](x: T) extends Conc[T] {
  override def level: Int = 0
  override def size: Int = 1

  override def left: Conc[T] = sys.error("single conc")
  override def right: Conc[T] = sys.error("single conc")
}

/**
  * Node tree of Conc implementation.
  * Inner Node is not allow to contain Empty as one of its subtrees.
  * The level difference between left and right is always <= 1.
  *
  * @param left left subtree
  * @param right right subtree
  */
case class <>[T](left: Conc[T], right: Conc[T]) extends Conc[T] {
  val level = 1 + math.max(left.level, right.level)
  val size = left.size + right.size
}

/**
  * It is used for appending elements. It does not have to be balanced.
  *
  * @param left left subtree
  * @param right right subtree
  */
case class Append[T](left: Conc[T], right: Conc[T]) extends Conc[T] {
  val level = 1 + math.max(left.level, right.level)
  val size = left.size + right.size
}

/**
  * Used to push the array into the conc tree.
  *
  * @param array array
  * @param size array size
  */
class Chunk[T](val array: Array[T], val size: Int) extends Conc[T] {
  def level = 0

  override def left: Conc[T] = sys.error("chunk conc")
  override def right: Conc[T] = sys.error("chunk conc")
}

object Conc {

  /**
    * It runs in a constant time. But the tree created this way is not balanced.
    * It is not suitable for parallelism.
    *
    * @param xs tree to append to
    * @param y element to append
    * @return appended tree
    */
  def appendLeafNotBalanced[T](xs: Conc[T], y: T): Conc[T] = Append(xs, new Single[T](y))

  def appendLeaf[T](xs: Conc[T], ys: Single[T]): Conc[T] =  xs match {
    case Empty => ys
    case xs : Single[T] => new <>(xs, ys)
    case _ <> _ => new Append[T](xs, ys)
    case xs: Append[T] => append(xs, ys)
  }

  /**
    * Appends Chunk leaf to the tree
    *
    * @param xs tree to append to
    * @param ys chunk leaf to append
    * @return appended tree
    */
  def appendLeaf[T](xs: Conc[T], ys: Chunk[T]): Conc[T] =  xs match {
    case Empty => ys
    case xs : Single[T] => new <>(xs, ys)
    case _ <> _ => new Append[T](xs, ys)
    case xs: Append[T] => append(xs, ys)
  }

  @tailrec private def append[T](xs: Append[T], ys: Conc[T]): Conc[T] = {
    if (xs.right.level > ys.level) new Append(xs, ys)
    else {
      val zs = new <>(xs.right, ys)
      xs.left match {
        case ws @ Append(_, _) => append(ws, zs)
        case ws if ws.level <= zs.level => ws <> zs
        case ws => new Append(ws, zs)
      }
    }
  }

}

object ConcRunner {


}