package silverstar
package coursera.parallel.programming
package week2

import common._

/**
  * Parallel Reduce.
  */
object ParallelReduce {

  val testList: List[Int] = List(1, 3, 8)
  val resultFoldLeft: Int = testList.foldLeft(100)((s, elem) => s - elem)
  // (((100 -1) - 3) - 8)
  val resultFoldRight: Int = testList.foldRight(100)((s, elem) => s - elem)
  // (1 - (3 - (8 - 100)))
  val resultReduceLeft: Int = testList.reduceLeft((s, elem) => s - elem)
  // ((1 - 3) - 8)
  val resultReduceRight: Int = testList.reduceRight((s, elem) => s - elem) // (1 - (3 - 8))

  def reduce[A](input: Array[A], f: (A, A) => A): A = {

    val threshold: Int = 4

    def reduceSeg(input: Array[A], left: Int, right: Int, f: (A, A) => A): A = {
      if (right - left < threshold) {
        var res = input(left)
        var i = left + 1
        while (i < right) {
          res = f(res, input(i))
          i = i + 1
        }
        res
      } else {
        val mid = left + (right - left) / 2
        val (a1, a2) = parallel(reduceSeg(input, left, mid, f), reduceSeg(input, mid, right, f))
        f(a1, a2)
      }
    }

    reduceSeg(input, 0, input.length, f)
  }

  def main(args: Array[String]) {
    println(s"testList=$testList")
    println(s"resultFoldLeft=$resultFoldLeft")
    println(s"resultFoldRight=$resultFoldRight")
    println(s"resultReduceLeft=$resultReduceLeft")
    println(s"resultReduceRight=$resultReduceRight")
  }
}
