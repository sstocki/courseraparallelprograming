package silverstar
package coursera.parallel.programming
package week4

import scala.collection.mutable.ArrayBuffer
import scala.reflect.ClassTag

import common.task

/**
  * Array combiner using parallel - two phase approach.
  */
class ArrayCombiner[T <: AnyRef : ClassTag](val parallelism: Int) {

  private var numElems: Int = 0
  private val buffers: ArrayBuffer[ArrayBuffer[T]] = new ArrayBuffer[ArrayBuffer[T]]
  buffers += new ArrayBuffer[T]

  /**
    * Finds the last nested array buffer in arrayBuffers and appends element x to it.
    * ArrayBuffer is expended when it is full and we want to append more elements.
    * Appending to ArrayBuffer takes constant time - O(1)
    *
    * @param x element to append
    * @return this
    */
  def +=(x: T): ArrayCombiner[T] = {
    buffers.last += x
    numElems += 1
    this
  }

  /**
    * Copies references of the that combiners buffer to its own buffers field.
    * No need of coping actual contents of its buffers just a pointer to it.
    *
    * @param that array combiner to be combined
    * @return combined array
    */
  def combine(that: ArrayCombiner[T]): ArrayCombiner[T] = {
    buffers ++= that.buffers
    numElems += that.numElems
    this
  }

  private def copyTo(array: Array[T], from: Int, end: Int): Unit = {
    var i = from
    var j = 0
    while(i >= buffers(j).length) {
      i -= buffers(j).length
      j += 1
    }
    var k = from
    while(k < end) {
      array(k) = buffers(j)(i)
      i += 1
      if(i >= buffers(j).length) {
        i = 0
        j += 1
      }
      k += 1
    }
  }

  def result: Array[T] = {
    val step = math.max(1, numElems / parallelism)
    val array = new Array[T](numElems)
    val starts = (0 until numElems by step) :+ numElems
    val chunks = starts.zip(starts.tail)
    val tasks = for ((from, end) <- chunks) yield task {
      copyTo(array, from, end)
    }
    tasks.foreach(_.join())
    array
  }
}

object ArrayCombinerRunner {

  def main(args: Array[String]) {

    val array: Array[String] = Array[String]("a", "r", "v", "w", "b", "s", "n")
    val combiner1: ArrayCombiner[String] = new ArrayCombiner[String](1)
    val combiner2: ArrayCombiner[String] = new ArrayCombiner[String](2)
    val combiner4: ArrayCombiner[String] = new ArrayCombiner[String](4)

    val combine1: Array[String] = array.aggregate(combiner1)(_ += _, _ combine _).result
    val combine2: Array[String] = array.aggregate(combiner2)(_ += _, _ combine _).result
    val combine4: Array[String] = array.aggregate(combiner4)(_ += _, _ combine _).result

    println(s"array=${array.mkString("; ")}")
    println(s"combine1=${combine1.mkString("; ")}")
    println(s"combine2=${combine2.mkString("; ")}")
    println(s"combine4=${combine4.mkString("; ")}")
  }
}
