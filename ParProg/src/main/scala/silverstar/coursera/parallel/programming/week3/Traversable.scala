package silverstar.coursera.parallel.programming.week3

/**
  * Traversable.
  */
trait Traversable[T] {

  def foreach(f: T => Unit): Unit

  def newBuilder: Builder[T, Traversable[T]]

  def filter(p: T => Boolean): Traversable[T] = {
    val b = newBuilder
    for(x <- this) if(p(x)) b += x
    b.result
  }
}
