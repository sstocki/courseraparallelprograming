package silverstar
package coursera.parallel.programming
package week4

/**
  * Builder. Used to implement sequential transformer operations. Where
  * T - type of the elements in the collection, e.g. String
  * Repr - type of the collection (representation), e.g. Seq[String]
  */
trait Builder[T, Repr] {

  /**
    * Add element to the builder.
    *
    * @param elem element to add
    * @return the builder
    */
  def += (elem: T): this.type

  /**
    * Returns the collection after all elements are added.
    *
    * @return collection of elements
    */
  def result: Repr
}
