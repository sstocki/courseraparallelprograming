package silverstar
package coursera.parallel.programming
package week2

import common._

/**
  * Created by sylwe on 18.06.2016.
  */
object ParallelSort {

  def parMergeSort(xs: Array[Int], maxDepth: Int): Unit = {

    val ys: Array[Int] = new Array[Int](xs.length)

    def merge(src: Array[Int], dst: Array[Int], from: Int, mid: Int, until: Int): Unit = ???

    def sort(from: Int, until: Int, depth: Int): Unit = {
      if (depth == maxDepth) {
        // quickSort
        xs.sorted
      } else {
        val mid = (from + until) / 2
        parallel(sort(mid, until, depth + 1), sort(from, mid, depth + 1))

        val flip = (maxDepth - depth) % 2 == 0
        val src = if(flip) ys else xs
        val dst = if(flip) xs else ys
        // merge
      }
    }
    sort(0, xs.length, 0)

  }
}
