package silverstar
package coursera.parallel.programming
package week3

/**
  * Parallel collections.
  */
object ParallelCollections {

  def f: Int = (1 until 1000).par.filter(_ % 3 == 0).count(n => n.toString == n.toString.reverse)

  /**
    * This function will always use seqential execution because of the nature of foldLeft operation.
    * There is no way of parallelizing it.
    *
    * @param xs array to sum
    * @return
    */
  def sum(xs: Array[Int]): Int = xs.par.foldLeft(0)(_ + _)

  def sumPar(xs: Array[Int]): Int = xs.par.fold(0)(_ + _)


  def maxPar(xs: Array[Int]): Int = xs.par.fold(Int.MinValue)((s, n) => if(n > s) n else s)


  //def foldLeft[A,B](f: (B, A) => B): B = ???
  //def fold[A](z: A)(f: (A,A) => A): A = ???

  /**
    * Play the game of rock, scissors, paper.
    *
    * @param a player a choice
    * @param b player b choice
    * @return result of single game
    */
  def play(a: String, b: String): String = List(a, b).sorted match {
    case List("paper", "scissors") => "scissors"
    case List("paper", "rock") => "paper"
    case List("rock", "scissors") => "rock"
    case List(aa, bb) if aa == bb => aa
    case List("", bb) => bb
  }

  /**
    * Result of parallel execution of the fold depends on the execution schedule.
    * Play function is commutative but NOT associative.
    * Function (play) must be associative and caled for a neutral element must form an identity function - MONOID.
    * If it is MONOID then it work fine with fold.
    *
    * @param a array of game choices
    * @return
    */
  def playGame(a: Array[String]): String = a.par.fold("")(play)

  //def aggregate[A,B](z: B)(f: (B,A) => B, g: (B,B) => B): B = ???

  /**
    * We cannot use fold, because we work with two different data types.
    * We need to use aggregate operation.
    * MONOID: { 0, _ + _ }
    *
    * @param a array of characters to count vowel for
    * @return
    */
  def countVowels(a: Array[Char]): Int = a.par.aggregate(0)(
    (count, c) => if(isVowel(c)) count + 1 else count,
    _ + _
  )


  def isVowel(c: Char): Boolean = c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u'

  def main(args: Array[String]) {

    val testArray: Array[Int] = Array(3, 4, 5)

    println(s"testArray=${testArray.mkString(";")}; sum = ${sum(testArray)}")
    println(s"testArray=${testArray.mkString(";")}; sumPar = ${sumPar(testArray)}")
    println(s"testArray=${testArray.mkString(";")}; maxPar = ${maxPar(testArray)}")
    println(s"f=$f")

    val game1 = Array("paper", "rock", "paper", "scissors")
    println(s"game1=${game1.mkString(";")}; winner = ${playGame(game1)}")
    println(s"game1=${game1.mkString(";")}; winner = ${playGame(game1)}")
    println(s"game1=${game1.mkString(";")}; winner = ${playGame(game1)}")

    val letters: Array[Char] = "test mkaopire".toCharArray
    println(s"letters=${letters.mkString(";")}; vowelsCount = ${countVowels(letters)}")
  }
}
