package silverstar
package coursera.parallel.programming
package week4

/**
  * Array combine example - inefficient O(n).
  */
object ArrayCombinerOnePhase {

  /**
    * Combines two arrays.
    *
    * @param xs first array to combine
    * @param ys second array to combine
    * @return combined array
    */
  def combine(xs: Array[Int], ys: Array[Int]): Array[Int] = {
    val r = new Array[Int](xs.length + ys.length)
    Array.copy(xs, 0, r, 0, xs.length)
    Array.copy(ys, 0, r, xs.length, ys.length)
    r
  }

  def main(args: Array[String]) {

    val a1 = Array[Int](1, 4, 5, 7, 9, 2)
    val a2 = Array[Int](3, 4, 6, 12, 1, 19)

    val a1a2 = combine(a1,a2)
    val a2a1 = combine(a2,a1)

    println(s"a1=${a1.mkString(";")}")
    println(s"a2=${a2.mkString(";")}")
    println(s"a1a2=${a1a2.mkString(";")}")
    println(s"a2a1=${a2a1.mkString(";")}")
  }
}
