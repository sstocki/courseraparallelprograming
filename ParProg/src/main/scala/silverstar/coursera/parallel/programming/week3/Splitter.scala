package silverstar
package coursera.parallel.programming
package week3

import java.util.concurrent.ForkJoinTask

import common._

/**
  * Splitter.
  */
trait Splitter[T] extends Iterator[T] {

  val threshold: Int

  def split: Seq[Splitter[T]]

  def remaining: Int

  def fold(z: T)(f: (T,T) => T): T = {
    if(remaining < threshold) foldLeft(z)(f)
    else {
      val children: Seq[ForkJoinTask[T]] = for(child <- split) yield task { child.fold(z)(f) }
      children.map(_.join()).foldLeft(z)(f)
    }
  }
}
