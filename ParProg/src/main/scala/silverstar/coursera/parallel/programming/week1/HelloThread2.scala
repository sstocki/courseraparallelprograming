package silverstar
package coursera.parallel.programming
package week1

/**
  * Hello therad2.
  */
class HelloThread2 extends Thread {

  override def run(): Unit = {
    println("Hello")
    println("world!")
  }
}

object HelloThread2 {

  def main(args: Array[String]) {
    val t = new HelloThread2
    val s = new HelloThread2

    t.start()
    s.start()
    t.join()
    s.join()
  }
}
