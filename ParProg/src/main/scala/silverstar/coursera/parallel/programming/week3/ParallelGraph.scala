package silverstar
package coursera.parallel.programming
package week3

import scala.collection.mutable
import scala.collection.concurrent.TrieMap

/**
  * Parallel Graph.
  */
object ParallelGraph {

  /**
    * Two errors:
    * modification of the collection taht is iterated in parallel
    * reading form collection that is concurrently modified
    * Mutable map is not a thread-safe collection.
    *
    * @param n number of elements in the graph
    * @return violations
    */
  def parGraphNotWorking(n: Int): Option[(Int, Int)] = {
    // never modify parallel collection if data-parallel operation is in progress
    // cyclic graph
    val graph = mutable.Map[Int, Int]() ++= (0 until n).map(i => (i, i + 1))
    graph(graph.size - 1) = 0

    // each graph node is replaced by its successors successor - computed in parallel
    for ((k, v) <- graph.par) graph(k) = graph(v)

    val violation: Option[(Int, Int)] = graph.find({ case (i, v) => v != (i + 2) % graph.size })
    violation
  }

  def parGraph(n: Int): Option[(Int, Int)] = {
    // never modify parallel collection if data-parallel operation is in progress
    // cyclic graph
    val graph = TrieMap[Int, Int]() ++= (0 until n).map(i => (i, i + 1))
    graph(graph.size - 1) = 0
    // efficient way of creating map copy
    val previous = graph.snapshot()
    // each graph node is replaced by its successors successor - computed in parallel
    // graph is atomicly captured when the for loop starts
    for ((k, v) <- graph.par) graph(k) = previous(v)

    val violation: Option[(Int, Int)] = graph.find({ case (i, v) => v != (i + 2) % graph.size })
    violation
  }

  def main(args: Array[String]) {

    val n: Int = 500000
    println(s"violationNotWorking=${parGraphNotWorking(n)}")
    println(s"violation=${parGraph(n)}")
    println(s"violation=${parGraph(n)}")
    println(s"violation=${parGraph(n)}")
    println(s"violation=${parGraph(n)}")

  }

}
