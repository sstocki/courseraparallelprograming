package silverstar
package coursera.parallel.programming
package week3

/**
  * Iterator.
  */
trait Iterator[T] {

  def hasNext: Boolean

  def next(): T

  def foldLeft[S](z: S)(f: (S, T) => S): S = {
    var result = z
    while(hasNext) result = f(result, next())
    result
  }
}
