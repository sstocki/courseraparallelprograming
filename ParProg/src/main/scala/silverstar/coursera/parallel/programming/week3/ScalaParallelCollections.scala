package silverstar
package coursera.parallel.programming
package week3

import scala.collection.{GenSeq, GenSet, mutable}
import java.util.concurrent.ConcurrentSkipListSet

import common._

/**
  * Scala Parallel Collections.
  */
object ScalaParallelCollections {

  /**
    * Finds biggest palindrome among numbers from collection xs.
    * Scala collection GenSeq can be executed either sequentially or in parallel.
    * It depends on the implementation of collection xs (Seq or ParSeq)
    *
    * @param xs sequence on numbers
    * @return
    */
  def largestPalindrome(xs: GenSeq[Int]): Int = {
    xs.aggregate(Int.MinValue)(
      (largest, n) => if (n > largest && n.toString == n.toString.reverse) n else largest,
      math.max
    )
  }

  /**
    * Scala mutable.Set is not thread safe class.
    * Avoid modifying same memory location without proper synchronization !!!
    *
    * @param a first set to intersect
    * @param b second set to intersect
    * @return result of intersection
    */
  def intersectionNotSafe(a: GenSet[Int], b: GenSet[Int]): Set[Int] = {
    val result = mutable.Set[Int]()
    for (x <- a) if (b.contains(x)) result += x
    result.toSet
  }

  def intersectionConcurrent(a: GenSet[Int], b: GenSet[Int]): ConcurrentSkipListSet[Int] = {
    val result = new ConcurrentSkipListSet[Int]()
    for (x <- a) if (b.contains(x)) result.add(x)
    result
  }

  def intersection(a: GenSet[Int], b: GenSet[Int]): GenSet[Int] = {
    if(a.size < b.size) a.filter(b(_))
    else b.filter(a(_))
  }

  def main(args: Array[String]) {

    val seq1: Set[Int] = (0 until 10000).toSet
    val seq2: Set[Int] = (0 until 10000 by 4).toSet
    val intersectionNotSafeSeqResult: Set[Int] = time {
      intersectionNotSafe(seq1, seq2)
    }
    println(s"intersectionNotSafeSeqResult=${intersectionNotSafeSeqResult.size}")
    val intersectionNotSafeParResult: Set[Int] = time {
      intersectionNotSafe(seq1.par, seq2.par)
    }
    println(s"intersectionNotSafeParResult=${intersectionNotSafeParResult.size}")

    val intersectionConcurrentSeqResult: ConcurrentSkipListSet[Int] = time {
      intersectionConcurrent(seq1, seq2)
    }
    println(s"intersectionConcurrentSeqResult=${intersectionConcurrentSeqResult.size}")
    val intersectionConcurrentParResult: ConcurrentSkipListSet[Int] = time {
      intersectionConcurrent(seq1.par, seq2.par)
    }
    println(s"intersectionConcurrentParResult=${intersectionConcurrentParResult.size}")

    val intersectionSeqResult: GenSet[Int] = time {
      intersection(seq1, seq2)
    }
    println(s"intersectionSeqResult=${intersectionSeqResult.size}")
    val intersectionParResult: GenSet[Int] = time {
      intersection(seq1.par, seq2.par)
    }
    println(s"intersectionParResult=${intersectionParResult.size}")

    val testArray: Array[Int] = (0 until 1000000).toArray

    val result: Int = time {
      largestPalindrome(testArray)
    }
    println(s"result=$result")

    val resultPar: Int = time {
      largestPalindrome(testArray.par)
    }
    println(s"resultPar=$resultPar")
  }

}
