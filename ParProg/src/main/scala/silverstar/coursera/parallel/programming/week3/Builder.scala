package silverstar
package coursera.parallel.programming
package week3

/**
  * Builder.
  */
trait Builder[A, Repr] {

  def +=(elem: A): Builder[A, Repr]

  def result: Repr
}
