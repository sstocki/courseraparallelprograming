package silverstar
package coursera.parallel.programming
package week2

import common._

/**
  * ParallelScan.
  */
object ParallelScan {

  def scanLeftSeq[A](input: Array[A], initial: A, f: (A, A) => A, output: Array[A]): Unit = {
    output(0) = initial
    var a = initial
    var i = 0
    while (i < input.length) {
      a = f(a, input(i))
      i = i + 1
      output(i) = a
    }
  }

  sealed abstract class Tree[A]

  case class Leaf[A](a: A) extends Tree[A]

  case class Node[A](left: Tree[A], right: Tree[A]) extends Tree[A]

  sealed abstract class TreeRes[A] {
    val res: A
  }

  case class LeafRes[A](override val res: A) extends TreeRes[A]

  case class NodeRes[A](left: TreeRes[A], override val res: A, right: TreeRes[A]) extends TreeRes[A]

  sealed abstract class TreeResA[A] {
    val res: A
  }

  case class LeafResA[A](from: Int, to: Int, override val res: A) extends TreeResA[A]

  case class NodeResA[A](left: TreeResA[A], override val res: A, right: TreeResA[A]) extends TreeResA[A]

  def reduceRes[A](tree: Tree[A], f: (A, A) => A): TreeRes[A] = tree match {
    case Leaf(v) => LeafRes(v)
    case Node(l, r) =>
      val (treeLeft, treeRight) = (reduceRes(l, f), reduceRes(r, f))
      NodeRes(treeLeft, f(treeLeft.res, treeRight.res), treeRight)
  }

  def upsweep[A](tree: Tree[A], f: (A, A) => A): TreeRes[A] = tree match {
    case Leaf(v) => LeafRes(v)
    case Node(l, r) =>
      val (treeLeft, treeRight) = parallel(upsweep(l, f), upsweep(r, f))
      NodeRes(treeLeft, f(treeLeft.res, treeRight.res), treeRight)
  }

  def downsweep[A](treeRes: TreeRes[A], initial: A, f: (A, A) => A): Tree[A] = treeRes match {
    case LeafRes(a) => Leaf(f(initial, a))
    case NodeRes(l, _, r) =>
      val (treeLeft, treeRight) = parallel(downsweep(l, initial, f), downsweep(r, f(initial, l.res), f))
      Node(treeLeft, treeRight)
  }

  def prepend[A](a: A, tree: Tree[A]): Tree[A] = tree match {
    case Leaf(v) => Node(Leaf(a), Leaf(v))
    case Node(l, r) => Node(prepend(a, l), r)
  }

  def scanLeft[A](tree: Tree[A], initial: A, f: (A, A) => A): Tree[A] = {
    val treeRes: TreeRes[A] = upsweep(tree, f)
    val resultTree: Tree[A] = downsweep(treeRes, initial, f)
    prepend(initial, resultTree)
  }

  def reduceSeg1[A](input: Array[A], left: Int, right: Int, initial: A, f: (A, A) => A): A = {
    var a = initial
    var i = left
    while (i < right) {
      a = f(a, input(i))
      i = i + 1
    }
    a
  }

  val threshold: Int = 5

  def upsweep[A](input: Array[A], from: Int, to: Int, f: (A, A) => A): TreeResA[A] = {
    if (to - from < threshold) LeafResA(from, to, reduceSeg1(input, from + 1, to, input(from), f))
    else {
      val mid = from + (to - from) / 2
      val (treeLeft, treeRight) = parallel(upsweep(input, from, mid, f), upsweep(input, mid, to, f))
      NodeResA(treeLeft, f(treeLeft.res, treeRight.res), treeRight)
    }
  }

  def downsweep[A](input: Array[A], initial: A, f: (A, A) => A, treeResA: TreeResA[A], output: Array[A]): Unit = treeResA match {
    case LeafResA(from, to, res) => scanLeftSegSeq(input, from, to, initial, f, output)
    case NodeResA(l, _, r) =>
      val (_, _) = parallel(downsweep(input, initial, f, l, output), downsweep(input, f(initial, l.res), f, r, output))
  }

  def scanLeftSegSeq[A](input: Array[A], left: Int, right: Int, initial: A, f: (A, A) => A, output: Array[A]): Unit = {
    if (left < right) {
      var i = left
      var a = initial
      while (i < right) {
        a = f(a, input(i))
        i = i + 1
        output(i) = a
      }
    }
  }

  def scanLeft[A](input: Array[A], initial: A, f: (A, A) => A, output: Array[A]): Unit = {
    val treeResA = upsweep(input, 0, input.length, f)
    downsweep(input, initial, f, treeResA, output)
    output(0) = initial
  }

  def main(args: Array[String]) {

    val testList: List[Int] = List[Int](1, 3, 8)
    println(s"testList=$testList")

    val resultScanLeft = testList.scanLeft(100)(_ + _)
    println(s"resultScanLeft=$resultScanLeft")

    val resultScanRight = testList.scanRight(100)(_ + _)
    println(s"resultScanRight=$resultScanRight")

    val tree1: Tree[Int] = Node(Node(Leaf(1), Leaf(3)), Node(Leaf(8), Leaf(50)))
    def plus = (x: Int, y: Int) => x + y
    val resultReduceRes: TreeRes[Int] = reduceRes(tree1, plus)
    val resultUpsweep: TreeRes[Int] = upsweep(tree1, plus)
    val resultDownsweep: Tree[Int] = downsweep(resultUpsweep, 100, plus)

    val resultScanLeftTree = scanLeft(tree1, 100, plus)


    val array1: Array[Int] = List[Int](1, 3, 8, 50).toArray
    val resultScanLeftArray = new Array[Int](array1.length + 1)
    scanLeft(array1, 100, plus, resultScanLeftArray)
    println(s"resultScanLeftArray=(${resultScanLeftArray.mkString("; ")})")
  }
}
