package silverstar
package coursera.parallel.programming
package week1

/**
  * Account.
  */
case class Account(uid: Long, private var amount: Int = 0) {

  def transfer(target: Account, amount: Int) = {
    if (this.uid < target.uid) this.transferDeadlock(target, amount)
    else target.transferDeadlock(this, -amount)
  }

  def transferDeadlock(target: Account, amount: Int) = {
    this.synchronized {
      target.synchronized {
        this.amount -= amount
        target.amount += amount
      }
    }
  }
}

object Account {

  private val x: AnyRef = new AnyRef {}
  private var uidCount: Long = 0L

  def getUniqueUid: Long = x.synchronized {
    uidCount = uidCount + 1
    uidCount
  }

  def startThreadDeadlock(fromAccount: Account, toAccount: Account, numberOfTransfers: Int, amount: Int = 10000) = {
    val t = new Thread {
      override def run(): Unit = {
        for (i <- 0 until numberOfTransfers) {
          fromAccount.transferDeadlock(toAccount, amount)
        }
      }
    }
    t.start()
    t
  }

  def startThread(fromAccount: Account, toAccount: Account, numberOfTransfers: Int, amount: Int = 10000) = {
    val t = new Thread {
      override def run(): Unit = {
        for (i <- 0 until numberOfTransfers) {
          fromAccount.transfer(toAccount, amount)
        }
      }
    }
    t.start()
    t
  }

  def main(args: Array[String]) {

    println("MAIN start")
    val account1: Account = new Account(getUniqueUid, 150000)
    val account2: Account = new Account(getUniqueUid, 450000)

    //    val t1 = startThreadDeadlock(account1, account2, 4)
    //    val t2 = startThreadDeadlock(account2, account1, 4)
    //    t1.join()
    //    t2.join()

    val t1 = startThread(account1, account2, 4)
    val t2 = startThread(account2, account1, 4)
    t1.join()
    t2.join()
  }
}
