package silverstar
package coursera.parallel.programming
package week1

import common._

/**
  * PNorm - generalization of notion of length from geometry.
  */
class PNorm {

  private def power(x: Int, exponent: Double): Int = math.pow(x.toDouble, exponent).toInt

  /** *
    * Returns sum of segment of an array a.
    *
    * @param a             array to calculate segment sum
    * @param exponent      exponent
    * @param leftBoundary  left boundary of the segment of array a - leftBoundary <= rightBoundary
    * @param rightBoundary right boundary of the segment of array a
    * @return sum of segment of array a
    */
  def sumSegment(a: Array[Int], exponent: Double, leftBoundary: Int, rightBoundary: Int): Int = {
    // TODO optimize the code
    var i: Int = leftBoundary
    var sum: Int = 0
    while (i < rightBoundary) {
      sum = sum + power(a(i), exponent)
      i = i + 1
    }
    sum
  }

  def pNorm(a: Array[Int], exponent: Double): Int = power(sumSegment(a, exponent, 0, a.length), 1 / exponent)

  /**
    * Sequential version of pNorm with dividing array a into two parts.
    *
    * @param a array on which the pNorm is calculated
    * @param exponent exponent
    * @return pNorm of an array a
    */
  def pNormTwoPart(a: Array[Int], exponent: Double): Int = {
    val middle: Int = a.length / 2
    val (sum1, sum2) = (sumSegment(a, exponent, 0, middle), sumSegment(a, exponent, middle, a.length))
    power(sum1 + sum2, 1 / exponent)
  }

  /**
    * PArallel version of pNorm with dividing array a into two parts.
    *
    * @param a array on which the pNorm is calculated
    * @param exponent exponent
    * @return pNorm of an array a
    */
  def pNormTwoPartPar(a: Array[Int], exponent: Double): Int = {
    val middle: Int = a.length / 2
    val (sum1, sum2) = parallel(sumSegment(a, exponent, 0, middle), sumSegment(a, exponent, middle, a.length))
    power(sum1 + sum2, 1 / exponent)
  }
}

object PNorm {

  def main(args: Array[String]) {
    val array: Array[Int] = Array(1, 4, 5, 2)
    val p: PNorm = new PNorm
    val pSeg1: Int = p.sumSegment(array, 1d, 0, 3)
    val pSeg2: Int = p.sumSegment(array, 1d, 0, 4)

    println(s"pSeg1=$pSeg1")
    println(s"pSeg2=$pSeg2")

    print("pNorm\t\t\t\t")
    val pNorm: Int = time {
      p.pNorm(array, 1d)
    }

    print("pNormTwoPart\t\t")
    val pNormTwoPart: Int = time {
      p.pNormTwoPart(array, 1d)
    }
    print("pNormTwoPartPar\t\t")
    val pNormTwoPartPar: Int = time {
      p.pNormTwoPartPar(array, 1d)
    }

    println(s"pNorm=$pNorm")
    println(s"pNormTwoPart=$pNormTwoPart")
    println(s"pNormTwoPartPar=$pNormTwoPartPar")
  }
}
