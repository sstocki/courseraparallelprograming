package silverstar
package coursera.parallel.programming
package week2

import common._

/**
  * Tree reduce.
  */
object TreeReduce {

  sealed abstract class Tree[A]
  case class Leaf[A](value: A) extends Tree[A]
  case class Node[A](left: Tree[A], right: Tree[A]) extends Tree[A]

  def reduceSeq[A](tree: Tree[A], f: (A,A) => A): A = tree match {
    case Leaf(v) => v
    case Node(l, r) => f(reduceSeq[A](l, f), reduceSeq[A](r, f))
  }

  def reducePar[A](tree: Tree[A], f: (A,A) => A): A = tree match {
    case Leaf(v) => v
    case Node(l, r) =>
      val (lV, rV) = parallel(reduceSeq[A](l, f), reduceSeq[A](r, f))
      f(lV, rV)
  }

  def toList[A](tree: Tree[A]): List[A] = tree match {
    case Leaf(v) => List(v)
    case Node(l, r) => toList[A](l) ++ toList[A](r)
  }

  def mapSeq[A, B](tree: Tree[A], f : A => B): Tree[B] = tree match {
    case Leaf(v) => Leaf(f(v))
    case Node(l, r) => Node(mapSeq[A, B](l, f), mapSeq[A, B](r, f))
  }

  def mapPar[A, B](tree: Tree[A], f : A => B): Tree[B] = tree match {
    case Leaf(v) => Leaf(f(v))
    case Node(l, r) =>
      val (left, right) = (mapPar[A, B](l, f), mapPar[A, B](r, f))
      Node(left, right)
  }

  def toListMapReduceSeq[A](tree: Tree[A]): List[A] = reduceSeq[List[A]](
    mapSeq[A, List[A]](tree, x => List[A](x)),
    (a: List[A], b: List[A]) => a ++ b
  )

  def toListMapReducePar[A](tree: Tree[A]): List[A] = reducePar[List[A]](
    mapPar[A, List[A]](tree, x => List[A](x)),
    (a: List[A], b: List[A]) => a ++ b
  )

  def main(args: Array[String]) {

    def tree: Tree[Int] = Node(Leaf(1), Node(Leaf(3), Leaf(8)))
    println(s"tree.toList=${toList(tree)}")
    println(s"tree.toListMapReduceSeq=${toListMapReduceSeq(tree)}")
    println(s"tree.toListMapReducePar=${toListMapReducePar(tree)}")

    def resultSeq = reduceSeq[Int](tree, _ - _)
    def resultPar = reduceSeq[Int](tree, _ - _)
    println(s"resultSeq=$resultSeq")
    println(s"resultPar=$resultPar")

    val r: Tree[List[Int]] = mapSeq[Int, List[Int]](tree, List[Int](_))
    println(s"r=$r")
  }
}
