package silverstar
package coursera.parallel.programming
package week4

/**
  * Combiner. Implements parallel transformer operations.
  */
trait Combiner[T, Repr] extends Builder[T, Repr] {

  /**
    * Takes given combiner and combines it with another combiner.
    *
    * @param that combiner to combine
    * @return combined combiner
    */
  def combine(that: Combiner[T, Repr]): Combiner[T, Repr]
}
