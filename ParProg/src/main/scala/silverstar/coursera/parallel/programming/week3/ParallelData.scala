package silverstar
package coursera.parallel.programming
package week3

/**
  * Parallel data.
  */
object ParallelData {

  /**
    * Initializes array with a given value v. Executes in parallel using parallel range (.par).
    *
    * @param xs array to be initialized
    * @param v value used for initialization
    */
  def initializeArray(xs: Array[Int])(v: Int): Unit = {
    for(i <- xs.indices.par){
      xs(i) = v
    }
  }

  /**
    * We approximate the definition of Mandelbrot Set.
    * As long as the absolute value of complex number zn is less than 2, we compute zn+1 until we reach maxIterations.
 *
    * @param xc real part of complex number
    * @param yc imaginary part of complex number
    * @param maxIterations maximum number of iterations
    * @return color representing the membership to the Mandelbrot Set
    */
  def computePixel(xc: Double, yc: Double, maxIterations: Int): Int = {
    var i = 0
    var x, y = 0.0
    while(x*x + y+y < 4 && i < maxIterations) {
      val xt = x*x-y* y+xc
      val yt = 2*x*y+yc
      x  = xt
      y = yt
      i += 1
    }
    i
  }

  def parRender(image: Array[(Double, Double)], output: Array[Int]): Unit = {
    //for(index <- (0 until image.length).par) {
    for(index <- image.indices.par) {
      output(index) = computePixel(image(index)._1, image(index)._2, 200)
    }
  }

  def main(args: Array[String]) {

  }
}
