package silverstar
package coursera.parallel.programming
package week1

import scala.collection.immutable.IndexedSeq

/**
  * Synchronization.
  */
object Synchronization {

  private val x: AnyRef = new AnyRef {}
  private var uidCount: Long = 0L

  def getUniqueId: Long = {
    uidCount = uidCount + 1
    uidCount
  }

  def getUniqueIdSync: Long = x.synchronized {
    uidCount = uidCount + 1
    uidCount
  }

  def startThread(): Thread = {
    val t = new Thread {
      override def run() = {
        val uids: IndexedSeq[Long] = for (i <- 0 until 10) yield getUniqueId
        println(uids)
      }
    }
    t.start()
    t
  }

  def startThreadSync(): Thread = {
    val t = new Thread {
      override def run() = {
        val uids: IndexedSeq[Long] = for (i <- 0 until 10) yield getUniqueIdSync
        println(uids)
      }
    }
    t.start()
    t
  }

  def main(args: Array[String]) {

    println("unsynchronized")
    val t1 = startThread()
    val t2 = startThread()
    t1.join()
    t2.join()

    println("synchronized")
    val t3 = startThreadSync()
    val t4 = startThreadSync()
    t3.join()
    t4.join()
  }
}
